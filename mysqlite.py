import sqlite3


class DBcontroller:

    def __init__(self):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        # 사전 테이블 생성(있다면 생성하지않음)
        curs.execute('CREATE TABLE IF NOT EXISTS "vocavulary" ("id" text, "voca" text, "mean" BLOB, PRIMARY KEY("id","voca"))')
        curs.execute('CREATE TABLE IF NOT EXISTS "quiz" ("id" text,"voca" TEXT,PRIMARY KEY("id"))')
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()

    def insert(self, tuple_data):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        curs.execute('INSERT OR REPLACE INTO vocavulary VALUES(?, ?, ?)', tuple_data)
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()

    def delete(self, tuple_data):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        curs.execute('Delete FROM vocavulary WHERE id=? and voca=?', tuple_data)
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()

    def selectAll(self, id):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        curs.execute('SELECT * FROM vocavulary WHERE id=' + '"' + id + '"')
        result = []
        for row in curs:
            result.append(row)
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()

        return result

    def random(self, id):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        curs.execute('SELECT * FROM vocavulary WHERE id=' + '"' + id + '"' + 'ORDER BY RANDOM() LIMIT 1')
        result = curs.fetchone()
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()

        return result

    # def selectOne(self, voca):
    #     # DB 파일 연결
    #     conn = sqlite3.connect('./dictionary.db')
    #     # 커서(쿼리받는부분)
    #     curs = conn.cursor()
    #     curs.execute('SELECT * FROM vocavulary WHERE voca=' + '"' + voca + '"')
    #     result = curs.fetchone()
    #     # 변경사항 저장
    #     conn.commit()
    #     # 커넥션 close
    #     conn.close()
    #
    #     return result

    def insert_quiz(self, tuple_data):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        curs.execute('INSERT OR REPLACE INTO quiz VALUES(?, ?)', tuple_data)
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()

    def select_quiz(self, id):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        curs.execute('SELECT * FROM quiz WHERE id=' + '"' + id + '"')
        result = curs.fetchone()
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()

        return result

    def delete_quiz(self, id):
        # DB 파일 연결
        conn = sqlite3.connect('./dictionary.db')
        # 커서(쿼리받는부분)
        curs = conn.cursor()
        curs.execute('Delete FROM quiz WHERE id=' + '"' + id + '"')
        # 변경사항 저장
        conn.commit()
        # 커넥션 close
        conn.close()